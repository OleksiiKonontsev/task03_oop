
public class Goods implements Comparable<Goods> {
    double price;
    double weight;
    boolean isUsed;
    String name;

    public Goods(double price, double weight, boolean isUsed, String name) {
        this.price = price;
        this.weight = weight;
        this.isUsed = isUsed;
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public double getWeight() {
        return weight;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public String getName() {
        return name;
    }

    public int compareTo(Goods o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return "Goods{" +
                "price=" + price +
                ", weight=" + weight +
                ", isUsed=" + isUsed +
                ", name='" + name + '\'' +
                '}';
    }
}
