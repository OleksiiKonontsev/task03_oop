import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GoodsFilter {
    public List<Goods> sortByPrice(List<Goods> goods) {
        Collections.sort(goods, new PriceComperator());
        return goods;
    }

    public List<Goods> sortByWeight(List<Goods> goods) {
        Collections.sort(goods, new WeightComparator());
        return goods;
    }

    public List<Goods> selectUsed(List<Goods> products, boolean isUsed) {
        List<Goods> goodss = new ArrayList<>();
        for(Goods good: products){
            if (good.isUsed == isUsed){
                goodss.add(good);
            }
        }
        return goodss;
    }
}