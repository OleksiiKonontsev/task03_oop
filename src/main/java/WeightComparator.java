import java.util.Comparator;

public class WeightComparator implements Comparator<Goods> {

    public int compare(Goods o1, Goods o2) {

        return (int)(o1.getWeight()- o2.getWeight());
    }
}
