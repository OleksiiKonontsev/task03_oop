import java.util.ArrayList;
import java.util.List;

public class Menu {
    private ConsoleReader consoleReader;
    private ConsoleWriter consoleWriter;
    private Gipermarket gipermarket;
    private GoodsFilter goodsFilter;

    public Menu(ConsoleReader consoleReader, ConsoleWriter consoleWriter, Gipermarket gipermarket, GoodsFilter goodsFilter) {
        this.consoleReader = consoleReader;
        this.consoleWriter = consoleWriter;
        this.gipermarket = gipermarket;
        this.goodsFilter = goodsFilter;
    }

    public void selectProduct() {
        List<String> menu = new ArrayList<>();
        menu.add("1 - Plumbings");
        menu.add("2 - Staff");
        menu.add("3 - Househols");
        menu.forEach(el -> consoleWriter.print(el));
        int value = consoleReader.readValue("Select product");
        if (value == 1) {
            List<Goods> goodss = gipermarket.getPlumbings();
            goodss.forEach(el -> consoleWriter.print(el.toString()));
            selectFilter(goodss);
        } else if (value == 2) {
            List<Goods> goodss = gipermarket.getStaff();
            goodss.forEach(el -> consoleWriter.print(el.toString()));
            selectFilter(goodss);
        } else if (value == 3) {
            List<Goods> goodss = gipermarket.getHousehold();
            goodss.forEach(el -> consoleWriter.print(el.toString()));
            selectFilter(goodss);
        } else {
            consoleWriter.print("Incorrect value");
            selectProduct();
        }
    }

    public void selectFilter(List<Goods> goodss) {
        List<String> menu = new ArrayList<>();
        menu.add("1 - Sort by price");
        menu.add("2 - Sort by weight");
        menu.add("3 - Show Used");
        menu.add("3 - Show Unused");
        menu.forEach(el -> consoleWriter.print(el));
        int value = consoleReader.readValue("Select filter");
        if (value == 1) {
            goodss = goodsFilter.sortByPrice(goodss);
        } else if (value == 2) {
            goodss = goodsFilter.sortByWeight(goodss);
        } else if (value == 3) {
            goodss = goodsFilter.selectUsed(goodss, true);
        } else if (value == 4) {
            goodss = goodsFilter.selectUsed(goodss, false);
        } else {
            consoleWriter.print("Incorrect value");
            selectFilter(goodss);
        }
        goodss.forEach(el -> consoleWriter.print(el.toString()));
    }


    public static void main(String[] args) {
        Menu menu = new Menu(new ConsoleReader(), new ConsoleWriter(), new Gipermarket(), new GoodsFilter());
        menu.selectProduct();
    }
}
