import java.util.Comparator;


public class PriceComperator implements Comparator<Goods>{

    public int compare(Goods o1, Goods o2) {

        return (int)(o1.getPrice()- o2.getPrice());
    }
}
