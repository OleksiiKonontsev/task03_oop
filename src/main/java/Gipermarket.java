import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Gipermarket {
    private List<Goods> goodsList = new ArrayList<>();

    {
        goodsList.add(new Plumbing(500, 20, false, "Bowl"));
        goodsList.add(new Plumbing(10500, 150, true, "Bath"));
        goodsList.add(new Staff(200, 2.5, false, "Hammer"));
        goodsList.add(new Staff(700.99, 3.35, true, "Axe"));
        goodsList.add(new Staff(450.99, 3.00, false, "Axe"));
        goodsList.add(new Household(30000, 12.3, false, "TV"));
        goodsList.add(new Household(80009, 7.12, true, "Computer"));
        goodsList.add(new Household(5315, 1.2, false, "PS"));

    }

    public List<Goods> getPlumbings() {
        List<Goods> plumbings = new ArrayList<>();
        for (Goods goods : goodsList) {
            if (goods instanceof Plumbing) {
                plumbings.add(goods);
            }
        }
        Collections.sort(plumbings);
        return plumbings;
    }

    public List<Goods> getStaff() {
        List<Goods> staffs = new ArrayList<>();
        for (Goods goods : goodsList) {
            if (goods instanceof Staff) {
                staffs.add((Staff) goods);
            }
        }
        Collections.sort(staffs);
        return staffs;
    }

    public List<Goods> getHousehold() {
        List<Goods> households = new ArrayList<>();
        for (Goods goods : goodsList) {
            if (goods instanceof Household) {
                households.add((Household) goods);
            }
        }
        Collections.sort(households);
        return households;

    }
}