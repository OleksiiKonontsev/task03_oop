import java.util.InputMismatchException;
import java.util.Scanner;

public class ConsoleReader {

    int readValue(String message) {
        try {
            System.out.println(message);
            Scanner scanner = new Scanner(System.in);
            return scanner.nextInt();
        }catch (InputMismatchException e){
            System.out.println("Incorrect value");
            return readValue(message);
        }
    }

}
